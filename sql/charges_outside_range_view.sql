SELECT
  billing_subscription.id AS subscription_id,
  billing_subscription.description AS subscription_description,
  billing_invoice.id AS invoice_id,
  billing_invoice.invoice_number,
  billing_rated_transaction.description,
  billing_rated_transaction.usage_date,
  billing_rated_transaction.amount_without_tax,
  cat_price_plan_matrix.code AS price_plan_code,
  account_entity.code AS user_account_code
FROM billing_invoice
INNER JOIN billing_rated_transaction ON billing_rated_transaction.invoice_id = billing_invoice.id
INNER JOIN billing_subscription ON billing_subscription.id = billing_invoice.subscription_id
INNER JOIN cat_price_plan_matrix ON cat_price_plan_matrix.id = billing_rated_transaction.priceplan_id
INNER JOIN billing_user_account ON billing_user_account.id = billing_subscription.user_account_id
INNER JOIN account_entity ON account_entity.id = billing_user_account.id
WHERE billing_invoice.invoice_number IS NULL
  AND NOT (daterange(DATE(:START_PERIOD), DATE(:END_PERIOD)) @> DATE(billing_rated_transaction.usage_date))
LIMIT 20
;
