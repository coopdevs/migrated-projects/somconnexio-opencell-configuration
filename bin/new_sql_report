#!/usr/bin/env python3
#
# This script is meant to make it a bit easier to develop SQL Open Cell reports
# using their API
#
# It's been a pain in the ass to add reports due to Open Cell report's bad UX.
# We consistently spend creating the report from the UI twice the time it takes
# to develop the required SQL query.
#
# The solution we came up with was using this exact repo to keep track of the
# requests we instead make through the API but that has some other
# inconveniences, such as easily escaping a nicely formatted SQL query into
# a valid JSON body.
#
# This script removes those annoyances.
#
# Usage:
#
#   $ bin/new_sql_report \
#       sql/charges_outside_range_view.sql \ # input query
#       CHARGES_OUTSIDE_RANGE \ # name in OpenCell
#       "Invoice charges outside the specified period" # description in OpenCell
#
# You can then copy the resulting JSON and paste it into a Postman's request
# body
#
import sys
import json

query_file = sys.argv[1]
code = sys.argv[2]
description = sys.argv[3]

data = {
    "code": f"{code}",
    "description": f"{description}",
    "disabled": False,
    "scriptType": "SQL",
    "filenameFormat": f"{code.lower()}_[YYYY-MM-dd]",
    "category": "billing",
    "outputDir": "reports/billing",
    "sqlQuery": "",
    "params": {
        "START_PERIOD": "YYYY-MM-DD",
        "END_PERIOD": "YYYY-MM-DD"
    },
    "reportExtractResultType": "CSV"
}

query = open(query_file, "r").read()
data['sqlQuery'] = query

json_body = json.dumps(data, indent=4)

print(json_body)
